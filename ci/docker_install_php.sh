#!/bin/bash

echo "Installing dependencies for php version: ${PHP_BUILD_IMAGE_MAJOR_PHP_VERSION}"

if [ -z "${PHP_BUILD_IMAGE_MAJOR_PHP_VERSION}" ]; then
  echo "Failure to parse php version from first argument";

  exit 1
fi

set -euxo pipefail

# Install git (the php image doesn't have it) which is required by composer
# as well as zip and curl to speed things up and finally openssh-client to be able to deploy.
DEBIAN_FRONTEND=noninteractive
apt-get update -yqq >/dev/null 2>&1
apt-get install -yqq git zip curl openssh-client python3 unzip brotli gettext-base  >/dev/null 2>&1

# Install the required lib files
apt-get install -yqq \
    libpng-dev \
    libjpeg62-turbo-dev \
    libicu-dev \
    libpq-dev \
    libxml2-dev \
    libxslt-dev \
    libzip-dev \
    libgd3 \
    libgd-dev \
    libjpeg-dev \
    libfreetype6-dev \
    libbrotli-dev \
    libpq-dev \
    libffi-dev \
    libmagickwand-dev \
      >/dev/null 2>&1

# Install composer
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install xdebug
pecl install xdebug > /dev/null && docker-php-ext-enable xdebug

# Install aws-cli
curl https://awscli.amazonaws.com/awscli-exe-linux-$(uname -m).zip -o awscliv2.zip
unzip -qq awscliv2.zip
rm awscliv2.zip
(cd aws && ./install)
aws --version

# Compile brotli extension
(cd php-ext-brotli \
  && phpize > /dev/null \
  && ./configure --with-libbrotli > /dev/null \
  && make > /dev/null \
  && make install > /dev/null \
  && docker-php-ext-enable brotli)

function pecl_install() {
  set +e
  pecl install -o -f "$1" 2>&1 > /tmp/pecl_install.log \
  &&  rm -rf /tmp/pear \
  &&  docker-php-ext-enable "$1"
  set -e
  RESULT=$?
  if [[ $RESULT -ne 0 ]]; then
    echo /tmp/pecl_install.log
  fi
  rm /tmp/pecl_install.log
  return $RESULT
}

pecl_install redis
pecl_install imagick
sed -i '/disable ghostscript format types/,+6d' /etc/ImageMagick-6/policy.xml

# Configure extensions
if [ "7.3" == "${PHP_BUILD_IMAGE_MAJOR_PHP_VERSION}" ]; then
  docker-php-ext-configure gd --with-jpeg-dir=/usr/include/ --with-freetype-dir=/usr/include/ > /dev/null
else
  docker-php-ext-configure gd --with-jpeg=/usr/include/ --with-freetype=/usr/include > /dev/null
fi

# Install extensions
docker-php-ext-install calendar > /dev/null
docker-php-ext-install exif > /dev/null
docker-php-ext-install gd > /dev/null
docker-php-ext-install gettext > /dev/null
docker-php-ext-install intl > /dev/null
docker-php-ext-install mysqli > /dev/null
docker-php-ext-install opcache > /dev/null
docker-php-ext-install pgsql > /dev/null
docker-php-ext-install pdo_mysql > /dev/null
docker-php-ext-install pdo_pgsql > /dev/null
docker-php-ext-install shmop > /dev/null
docker-php-ext-install soap > /dev/null
docker-php-ext-install sockets > /dev/null
if [ "7.3" == "${PHP_BUILD_IMAGE_MAJOR_PHP_VERSION}" ]; then
  docker-php-ext-install wddx > /dev/null
  docker-php-ext-install xmlrpc > /dev/null
fi
docker-php-ext-install xsl > /dev/null
docker-php-ext-install zip > /dev/null

# Install gd, again, for some reason

if [ "7.3" == "${PHP_BUILD_IMAGE_MAJOR_PHP_VERSION}" ]; then
  docker-php-ext-configure gd --with-jpeg-dir=/usr/include/ --with-freetype-dir=/usr/include/ > /dev/null
else
  docker-php-ext-configure gd --with-jpeg=/usr/include/ --with-freetype=/usr/include > /dev/null
fi
docker-php-ext-install gd > /dev/null
