#!/bin/bash
set -euxo pipefail

function validate_parameter() {
  if [ -z "${!1}" ]; then
    echo "It would appear that $1 is not set"
    if [ "CI_REGISTRY_IMAGE" == "$1" ]; then
      echo "Not pushing new tags"
      CI_REGISTRY_IMAGE="local-php-build-image"
    else
      echo "Please ensure the following environment variables are set: "
      echo "  - CI_REGISTRY_IMAGE*"
      echo "  - PHP_VERSION"
      echo "  - DEBIAN_VERSION"
      echo "  - PHP_SAPI"
      echo "  - ARCH"
      echo "* only required for pushing"
      echo "Unable to start build script"
      exit 1
    fi
  else
    echo "Parameter $1 is set to: ${!1}"
  fi
}

set +x
for PARAMETER in PHP_VERSION DEBIAN_VERSION PHP_SAPI CI_REGISTRY_IMAGE ARCH; do
  validate_parameter "${PARAMETER}"
done



SIMPLE_TAG="${CI_REGISTRY_IMAGE}:${PHP_VERSION}"

if [ "${IMAGE_TEST_INFIX}" ]; then
  TAG="${SIMPLE_TAG}-${IMAGE_TEST_INFIX}-${DEBIAN_VERSION}-${ARCH}"
else
  echo "Unexpectedly missing IMAGE_TEST_INFIX, aborting..."
  TAG="${SIMPLE_TAG}-${DEBIAN_VERSION}-${ARCH}"
  exit 1
fi

echo "Starting build for: ${TAG}"
set -x

docker build -t "${TAG}" \
  --build-arg PHP_BUILD_IMAGE_MAJOR_PHP_VERSION="${PHP_VERSION}" \
  --build-arg PHP_BUILD_IMAGE_DEBIAN_CODENAME="${DEBIAN_VERSION}" \
  --build-arg PHP_BUILD_IMAGE_SAPI="${PHP_SAPI}" \
   .
set +x

if [ -z "${CI}" ]; then
  echo "Not pushing tags from a local machine"
  exit 0
fi
set -x
docker push "${TAG}"
# BC image tags
if [ "buster" == "${DEBIAN_VERSION}" ]; then
  docker tag "${TAG}" "${SIMPLE_TAG}"
  docker push "${SIMPLE_TAG}"
fi
